%define parse.error verbose
%define parse.trace

%code requires {
	#include <stdio.h>
	
	extern void yyerror(const char*);
	extern FILE *yyin;
}

%code {
	extern int yylex();
	extern int yylineno;
}

%union {
	char *string;
	double floatValue;
	int intValue;
}

%token AND           "&&"
%token OR            "||"
%token EQ            "=="
%token NEQ           "!="
%token LEQ           "<="
%token GEQ           ">="
%token LSS           "<"
%token GRT           ">"
%token KW_BOOLEAN    "bool"
%token KW_DO         "do"
%token KW_ELSE       "else"
%token KW_FLOAT      "float"
%token KW_FOR        "for"
%token KW_IF         "if"
%token KW_INT        "int"
%token KW_PRINTF     "printf"
%token KW_RETURN     "return"
%token KW_VOID       "void"
%token KW_WHILE      "while"
%token CONST_INT     "integer literal"
%token CONST_FLOAT   "float literal"
%token CONST_BOOLEAN "boolean literal"
%token CONST_STRING  "string literal"
%token ID            "identifier"

// definition of association and precedence of operators
%left '+' '-' OR
%left '*' '/' AND
%nonassoc UMINUS

// workaround for handling dangling else
// LOWER_THAN_ELSE stands for a not existing else
%nonassoc LOWER_THAN_ELSE
%nonassoc KW_ELSE

%%

program:
       | program assignment_function
	;
assignment_function: declassignment ";" | functiondefinition
		   ;
functiondefinition: type id '(' parameterlist_q ')' '{' statementlist '}'
		  ;
parameterlist_q: /* empty */
	       | parameterlist
	;
parameterlist: type id type_id_list
	     ;
type_id_list: /* empty */ 
	    | type_id_list ',' type id
	    ;
functioncall: id '(' assignmentlist_q ')'
	    ;
assignmentlist_q: /* empty */
	       | assignment assignment_list
		;
assignment_list: /* empty */
	       | assignment_list ',' assignment
	       ;
statementlist: /* empty */
	    | statementlist block  
	    ;
block: '{' statementlist '}' 
     | statement
     ;
statement: ifstatement 
	 | forstatement
	 | whilestatement
	 | returnstatement ';'
	 | dowhilestatement ';'
	 | printf ';'
	 | declassignment ';'
	 | statassignment ';'
	 | functioncall ';'
	 ;
statblock: '{' statementlist '}' | statement
	 ;
ifstatement: KW_IF '(' assignment ')' statblock else_q
	   ;
else_q: /* empty */
     KW_ELSE statblock 
      ;
forstatement: KW_FOR '(' decl_or_stat ';' expr ';' statassignment ')' statblock
	    ;
decl_or_stat: statassignment
	    | declassignment
	;
dowhilestatement: KW_DO statblock KW_WHILE '(' assignment ')'
		;
whilestatement: KW_WHILE '(' assignment ')' statblock
	      ;
returnstatement: KW_RETURN assignment_q
	       ;
assignment_q: /* empty*/
	    | assignment
	;
printf: KW_PRINTF '(' assignment_or_string ')'
      ;
assignment_or_string: assignment
		    | CONST_STRING
	;
declassignment: type id equals_assignment_q
	      ;
equals_assignment_q: /* empty */
		  | '=' assignment
	;
statassignment: id '=' assignment
	      ;
assignment: id '=' assignment 
	  | expr
	  ;
expr: simpexpr comp_simpexpr_q
    ;
comp_simpexpr_q: /* empty */
	       | comp_simpexpr
	;
comp_simpexpr: EQ simpexpr
	     | GRT simpexpr
	     | LSS simpexpr
	     | GEQ simpexpr
	     | LEQ simpexpr
	     | NEQ simpexpr
	;
simpexpr: minus_term_term op_term_list
	;
minus_term_term: '-' term
	       | term
	;
op_term_list: /* empty */
	    | op_term_list op_term
	;
op_term: '+' term
       | OR term
       | '-' term
	;
term: factor op_factor_list
	;
op_factor_list: /* empty */
	      | op_factor_list op_factor
	;
op_factor: '*' factor
	 | AND factor
	 | '/' factor
	;
factor: CONST_INT
      | '(' assignment ')' 
      | id
      | functioncall
      | CONST_BOOLEAN
      | CONST_FLOAT
	;
type: KW_BOOLEAN
    | KW_VOID
    | KW_INT
    | KW_FLOAT
	;
id: ID
  ;



%%

int main(int argc, char *argv[]) {
	yydebug = 0;

	if (argc < 2) {
		yyin = stdin;
	} else {
		yyin = fopen(argv[1], "r");
		if (yyin == 0) {
			printf("ERROR: Datei %s nicht gefunden", argv[1]);
		}
	}

	return yyparse();
}

void yyerror(const char *msg) {
	fprintf(stderr, "Line %d: %s\n", yylineno, msg);
}
